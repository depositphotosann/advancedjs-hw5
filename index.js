"use strict"

/*
Завдання

Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

Технічні вимоги:
При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:

https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts

Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

Необов'язкове завдання підвищеної складності
Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу:  https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
*/



/*
Ось загальний опис кроків, які потрібно виконати:
Отримання списку користувачів та публікацій:

Напишіть JavaScript-код, який відправлятиме GET-запити на сервер за адресами https://ajax.test-danit.com/api/json/users та https://ajax.test-danit.com/api/json/posts.
Обробіть отримані дані про користувачів та публікації.
Відображення публікацій на сторінці:

Створіть HTML-шаблон для кожної публікації.
З використанням JavaScript, вставте цей HTML-шаблон для кожної публікації на сторінці.
У кожній публікації додайте кнопку для видалення.
Видалення публікації:

Напишіть код JavaScript, який відправлятиме DELETE-запит на сервер при натисканні на кнопку видалення певної публікації.
Після успішного видалення, видаліть відповідну публікацію зі сторінки.
Додавання нової публікації:

Створіть кнопку "Додати публікацію", яка відкриє модальне вікно з формою для введення заголовка та тексту публікації.
Напишіть код JavaScript для відправлення POST-запиту на сервер з даними нової публікації.
Після успішного додавання, вставте нову публікацію зверху списку на сторінці.
Редагування публікації:

Додайте функціонал для редагування вмісту публікації.
При натисканні на кнопку редагування, відкрийте модальне вікно з формою для введення нового заголовка та тексту.
Напишіть код JavaScript, який відправлятиме PUT-запит на сервер з оновленими даними публікації.
Анімація завантаження (необов'язково):

Додайте CSS-анімацію, яка відображатимеся під час завантаження даних з сервера.
Це загальний підхід до вирішення вашого завдання. Для реалізації вам знадобиться знання HTML, CSS, JavaScript та використання HTTP-запитів. Не забудьте також про обробку помилок і валідацію даних з сервера.
*/

class Card {
  constructor(post, user) {
      this.post = post;
      this.user = user;
  }

  render() {
      const card = document.createElement('div');
      card.classList.add('card');

      const title = document.createElement('h2');
      title.textContent = this.post.title;

      const text = document.createElement('p');
      text.textContent = this.post.body;

      const userInfo = document.createElement('div');
      userInfo.textContent = `${this.user.name} ${this.user.username} (${this.user.email})`;

      const deleteButton = document.createElement('button');
      deleteButton.classList.add('delete');
      deleteButton.dataset.postid = this.post.id;
      deleteButton.textContent = 'Delete';
      deleteButton.addEventListener('click', () => this.deleteCard());

      card.appendChild(title);
      card.appendChild(text);
      card.appendChild(userInfo);
      card.appendChild(deleteButton);

      return card;
  }

  deleteCard() {
    document.addEventListener('click', event => {
      event.preventDefault();
      if (event.target.classList.contains('delete')) {
        const postId = +event.target.getAttribute('data-postid');
        if(postId === this.post.id){
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, { method: 'DELETE' })
          .then(() => {
            event.target.parentElement.remove();
          });
        }
      }
    });
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const mainContent = document.getElementById('mainContent');
  const loadingAnimation = document.getElementById('loadingAnimation');
  const addPostBtn = document.getElementById('addPostBtn');
  const postModal = document.getElementById('postModal');
  const closeBtn = document.getElementsByClassName('close')[0];
  const savePostBtn = document.getElementById('savePostBtn');
  const postTitleInput = document.getElementById('postTitle');
  const postContentInput = document.getElementById('postContent');

  addPostBtn.addEventListener('click', () => {
      postTitleInput.value = '';
      postContentInput.value = '';
      postModal.style.display = 'block';
  });

  closeBtn.addEventListener('click', () => {
      postModal.style.display = 'none';
  });

  savePostBtn.addEventListener('click', () => {
      const newPost = {
          title: postTitleInput.value,
          body: postContentInput.value,
          userId: 1,
          id: Date.now(),
      };

      fetch('https://ajax.test-danit.com/api/json/posts', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify(newPost)
      })
      .then(response => {
          if (response.ok) {
              return response.json();
          }
          throw new Error('Network response was not ok.');
      })
      .then(data => {
          postModal.style.display = 'none';
          fetch('https://ajax.test-danit.com/api/json/users/1')
            .then(response => {
                if (response.ok) {
                  return response.json();
            }
            throw new Error('Network response was not ok.');
          }).then((user) => {
            mainContent.insertBefore(new Card(newPost, user).render(), mainContent.firstChild);
          })
      })
      .catch(error => console.error('Error adding new post:', error));
  });

  fetch('https://ajax.test-danit.com/api/json/users')
  .then(response => {
      if (response.ok) {
          return response.json();
      }
      throw new Error('Network response was not ok.');
  })
  .then(users => {
      fetch('https://ajax.test-danit.com/api/json/posts')
      .then(response => {
          if (response.ok) {
              return response.json();
          }
          throw new Error('Network response was not ok.');
      })
      .then(posts => {
          loadingAnimation.classList.add('hidden');
          mainContent.classList.remove('hidden');

          posts.forEach(post => {
              const user = users.find(user => user.id === post.userId);
              const card = new Card(post, user);
              mainContent.appendChild(card.render());
          });
      })
      .catch(error => console.error('Error fetching posts:', error));
  })
  .catch(error => console.error('Error fetching users:', error));
});